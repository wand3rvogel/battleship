package battleship;

import java.util.ArrayList;
import java.util.Arrays;

public class BattleField {

    private final char[][] field;
    private ArrayList<Ship> ships;

    BattleField() {

        field = new char[10][10];

        for (int i = 0; i < 10; i++) {
            Arrays.fill(field[i], '~');
        }

        initShips();
    }

    private void initShips() {
        ships = new ArrayList<>();
        ships.add(new Ship("Aircraft Carrier", 5));
        ships.add(new Ship("Battleship", 4));
        ships.add(new Ship("Submarine", 3));
        ships.add(new Ship("Cruiser", 3));
        ships.add(new Ship("Destroyer", 2));
    }

    private void printWithShips(boolean withShips) {

        System.out.println("  1 2 3 4 5 6 7 8 9 10");
        for (int i = 0; i < 10; i++) {
            System.out.print((char) ('A' + i) + " ");
            for (int j = 0; j < 10; j++) {

                if (!withShips && field[i][j] == 'O') {
                    System.out.print("~ ");
                } else {
                    System.out.print(field[i][j] + " ");
                }
            }
            System.out.println();
        }
    }

    public void printFog() {
        printWithShips(false);
    }

    public void printAll() {
        printWithShips(true);
    }

    public void testShip(Ship ship, Coordinate[] coords) {

        int shipLength = ship.getLength();

        boolean sameHLine = coords[0].getY() == coords[1].getY();
        boolean sameVLine = coords[0].getX() == coords[1].getX();
        int xLength = Math.abs(coords[0].getX() - coords[1].getX()) + 1;
        int yLength = Math.abs(coords[0].getY() - coords[1].getY()) + 1;

        if (!sameHLine && !sameVLine) {
            throw new BattleFieldException("Wrong ship location!");
        }

        boolean isLengthOk = sameHLine && xLength == shipLength || sameVLine && yLength == shipLength;
        if (!isLengthOk) {
            throw new BattleFieldException("Wrong length of the " + ship.getName() + "!");
        }

        boolean allPointsAvailable = true;
        for (int i = coords[0].getY(); i <= coords[1].getY() && allPointsAvailable; i++) {
            for (int j = coords[0].getX(); j <= coords[1].getX(); j++) {
                if (!testPointAvailable(i, j)) {
                    allPointsAvailable = false;
                    break;
                }
            }
        }
        if (!allPointsAvailable) {
            throw new BattleFieldException("You placed it too close to another one.");
        }
    }

    private boolean testPointAvailable(int row, int col) {

        for (int i = -1; i <= 1; i++) {
            if (row + i < 0 || row + i > 9) {
                continue;
            }
            for (int j = -1; j <= 1; j++) {
                if (col + j < 0 || col + j > 9) {
                    continue;
                }
                if (field[row + i][col + j] != '~') {
                    return false;
                }
            }
        }
        return true;
    }


    public void placeShip(Ship ship, Coordinate[] coords) {

        for (int i = coords[0].getY(); i <= coords[1].getY(); i++) {
            for (int j = coords[0].getX(); j <= coords[1].getX(); j++) {
                field[i][j] = 'O';
                ship.addPart(new Coordinate(i, j));
            }
        }
    }

    public String attack(Coordinate shot) {

        int row = shot.getY();
        int col = shot.getX();

        String resultString = "";
        boolean hasHit = false;
        for (Ship ship : ships) {
            if (ship.getParts().contains(shot)) { // hit
                field[row][col] = 'X';
                if (allPartsKilled(ship)) {
                    ship.setKilled();
                    if (allShipsKilled()) {
                        resultString = "You sank the last ship. You won. Congratulations!";
                    } else {
                        resultString = "You sank a ship!";
                    }
                } else {
                    resultString = "You hit a ship!";
                }
                hasHit = true;
                break;
            }
        }
        if (!hasHit) { // miss
            field[row][col] = 'M';
            resultString = "You missed!";
        }

        return resultString;
    }
    private boolean allPartsKilled(Ship ship) {

        for (Coordinate part : ship.getParts()) {
            if (field[part.getY()][part.getX()] != 'X'){
                return false;
            }
        }
        return true;
    }

    public ArrayList<Ship> getShips() {
        return ships;
    }

    public boolean allShipsKilled() {

        for (Ship ship : ships) {
            if (!ship.isKilled()) {
                return false;
            }
        }
        return true;
    }
}
