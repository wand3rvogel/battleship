package battleship;

public class BattleFieldException extends RuntimeException {

    String message;

    BattleFieldException(String exceptionMessage) {
        super("Error! " + exceptionMessage + " Try again:");
    }
}
