package battleship;

import java.util.Objects;

public class Coordinate {

    private int y;
    private int x;

    Coordinate(int row, int col) {
        this.y = row;
        this.x = col;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public static Coordinate[] getTwoCoordinates(String str) {

        String[] splitted = str.split(" ");

        int point1row = splitted[0].charAt(0) - 'A';
        int point1col = Integer.parseInt(splitted[0].substring(1)) - 1;
        int point2row = splitted[1].charAt(0) - 'A';
        int point2col = Integer.parseInt(splitted[1].substring(1)) - 1;

        Coordinate point1 = new Coordinate(point1row, point1col);
        Coordinate point2 = new Coordinate(point2row, point2col);

        boolean wrongXSequence = point1row == point2row && point1col > point2col;
        boolean wrongYSequence = point1col == point2col && point1row > point2row;

        if (wrongXSequence || wrongYSequence) {
            return new Coordinate[]{point2, point1};
        } else {
            return new Coordinate[]{point1, point2};
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return y == that.y && x == that.x;
    }

    @Override
    public int hashCode() {
        return Objects.hash(y, x);
    }

    @Override
    public String toString() {
        return String.format("%s%d", (char) (y + 'A'), x + 1);
    }
}
