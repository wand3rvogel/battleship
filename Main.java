package battleship;

import java.util.Scanner;

public class Main {

    private static final Scanner scanner = new Scanner(System.in);
    private static Player[] players;

    public static void main(String[] args) {

        players = new Player[] {
                new Player("Player 1", new BattleField()),
                new Player("Player 2", new BattleField())
        };

        placeShips(players[0]);
        placeShips(players[1]);

        startGame();
    }

    private static void startGame() {

        int currentPlayer = 0;
        int enemy = 1;
        while (true) {
            showFields(enemy, currentPlayer);

            System.out.printf("%n%s, it's your turn:%n", players[currentPlayer].name);
            String result = players[enemy].field.attack(getShot());
            System.out.printf("%n%s%n", result);

            if (isGameOver()) {
                break;
            }
            waitPressEnter();

            currentPlayer = ++currentPlayer % 2;
            enemy = ++enemy % 2;
        }
    }

    private static boolean isGameOver() {
        return players[0].field.allShipsKilled() || players[1].field.allShipsKilled();
    }

    private static void showFields(int enemy, int currentPlayer) {
        players[enemy].field.printFog();
        System.out.println("-".repeat(21));
        players[currentPlayer].field.printAll();
    }

    private static Coordinate getShot() {

        String input = scanner.nextLine();
        while (!input.matches("^[A-J](\\d|10)$")) {
            System.out.println("Error! You entered the wrong coordinates! Try again:");
            input = scanner.nextLine();
        }
        return new Coordinate(input.charAt(0) - 'A', Integer.parseInt(input.substring(1)) - 1);
    }

    private static void placeShips(Player player) {

        System.out.println(player.name + ", place your ships on the game field\n");
        player.field.printAll();

        for (Ship ship : player.field.getShips()) {

            Coordinate[] coords = getShipCoordinates(player, ship);
            player.field.placeShip(ship, coords);
            player.field.printAll();
        }
        System.out.println();
        waitPressEnter();
    }

    private static void waitPressEnter() {
        System.out.println("Press Enter and pass the move to another player");
        String enter = scanner.nextLine();
    }

    private static Coordinate[] getShipCoordinates(Player player, Ship ship) {

        Coordinate[] coords = null;
        System.out.printf("%nEnter the coordinates of the %s (%d cells)%n", ship.getName(), ship.getLength());
        boolean isCoordsOk = false;
        while (!isCoordsOk) {
            coords = Coordinate.getTwoCoordinates(scanner.nextLine());

            try {
                player.field.testShip(ship, coords);
            } catch (BattleFieldException e) {
                System.out.println(e.getMessage());
                continue;
            }
            isCoordsOk = true;
        }
        return coords;
    }
}

