package battleship;

class Player {

    final String name;
    final BattleField field;

    Player(String name, BattleField field) {
        this.name = name;
        this.field = field;
    }
}
