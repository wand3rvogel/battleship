package battleship;

import java.util.ArrayList;

public class Ship {

    private final String name;
    private final int length;

    private final ArrayList<Coordinate> parts = new ArrayList<>();

    private boolean killed = false;

    public Ship(String name, int length) {
        this.name = name;
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }

    public ArrayList<Coordinate> getParts() {
        return parts;
    }

    public void addPart(Coordinate cell) {
        parts.add(cell);
    }

    public boolean isKilled() {
        return killed;
    }

    public void setKilled() {
        this.killed = true;
    }
}
